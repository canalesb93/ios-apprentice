iOS-Apprentice
==============

iOS excersice applications in swift from the book series, iOS Apprentice(3rd Edition) by Matthijs Hollemans.

* Book 1 - Getting Started: **Finished**
* Book 2 - Checklist:       **Started**
* Book 3 - MyLocations:     **Pending**
* Book 4 - StoreSearch:     **Pending**