//
//  ChecklistItem.swift
//  Checklists
//
//  Created by Ricardo Canales on 10/5/14.
//  Copyright (c) 2014 Ricardo Canales. All rights reserved.
//

import Foundation

class ChecklistItem: NSObject {
  var text = ""
  var checked = false
  
  func toggleChecked(){
    checked = !checked
  }
}